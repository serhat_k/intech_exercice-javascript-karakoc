function padLeft(char, str, quantity)
{
    length_str = str.length;
    length_char = char.length;

    i = 0;
    while(length_str < quantity)
    {
        str = char[i] + str;
        length_str = str.length;
        if(i+1 === length_char) i = 0; else i++;
    }
    return str
}

function padRight(char, str, quantity)
{
    length_str = str.length;
    length_char = char.length;

    i = 0;
    while(length_str < quantity)
    {
        str = str + char[i];
        length_str = str.length;
        if(i+1 === length_char) i = 0; else i++;
    }

    return str
}

const padZeros = (str, quantity) => padLeft("0", str, quantity);
const padSpaces = (str, quantity) => padRight(" ", str, quantity);
console.log(padLeft(".", "123", 6)); // => "000123"
console.log(padRight(".", "45", 8)); // => "45    
console.log(padZeros("123", 6));
console.log(padSpaces("45", 8))

