function contains(haystack, needle, startIndex = 0)
{
    len_hstack = haystack.length;
    len_need = needle.length;
    number_verif = 0;
    index_start_contains = 0;
    for(; startIndex < len_hstack; startIndex++)
    {
        if(needle[index_start_contains] === haystack[startIndex]) index_start_contains++; else index_start_contains = 0; 
        if (index_start_contains === len_need) return startIndex;
    }
    return -1;
}

console.log(contains("hello", "o"));
console.log(contains("hello", "1", 0));