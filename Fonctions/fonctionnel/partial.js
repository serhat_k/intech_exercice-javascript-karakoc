function partial(fName, ...Param){
    return (...ParamAutre) => { 
        return fName(...Param, ...ParamAutre);
    }
} 
const f = (x, y, z) => x * (y - z);
console.log(partial(f, 2)(3, 4)) // => -2   <=> (2 * (3 - 4))