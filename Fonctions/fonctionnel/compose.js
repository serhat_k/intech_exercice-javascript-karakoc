/* COMPOSE */
const increment = x => x + 1;
const double = y => y * 2;

function compose(f, g)
{
  return (v) => f(g(v));
}

const timesTwoPlusOne = compose(increment, double);

console.log(timesTwoPlusOne(5))

