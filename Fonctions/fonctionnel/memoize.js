// const memoize = () => {
//     let cache = {};
//     let result_new_fibo;
//     return (n) => {
//       if (n in cache) {
//         return cache[n];
//       }
//       else {
//         if (n === 0) {
//             result_new_fibo = 0;
//         } else if (n === 1) {
//             result_new_fibo = 1;
//         } else {
//             result_new_fibo = memoize(n - 1) + memoize(n - 2);
//         }
//         cache[n] = result_new_fibo;
//         return result_new_fibo;
//       }
//     }
//   }

const memoize = (fn) => {
    let cache = {};
    return (...args) => {
      let n = args[0]; 
      if (n in cache) {
        return cache[n];
      }
      else {
        let result = fn(n);
        cache[n] = result;
        return result;
      }
    }
  }


function fibonacci(n) {
	if (n === 0) {
		return 0;
	} else if (n === 1) {
		return 1;
	} else {
		return fibonacci(n - 1) + fibonacci(n - 2);
	}
}

const memoize_f = memoize(fibonacci);

console.log(fibonacci(40))
 console.log("###############")
console.log(memoize_f(40))
console.log(memoize_f(40))
console.log(memoize_f(40))
console.log(memoize_f(40))
console.log(memoize_f(40))
console.log("###############")
console.log(memoize_f(41))
console.log(memoize_f(41))