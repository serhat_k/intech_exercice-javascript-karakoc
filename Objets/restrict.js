function restrict(tooMuchConfig, config){
    objetReturn = {...config}
    for(let [key, value] of Object.entries(config))
    {
        if(tooMuchConfig[key] in tooMuchConfig) objetReturn[key] = tooMuchConfig[key]
    }

    return objetReturn
}

const config = { user: "user", pass: "pass" };
const tooMuchConfig = { vars: "LOG=info", user: "user", pass: "pass", env: "prod" };
console.log("vars" in tooMuchConfig); // => true
console.log("env" in tooMuchConfig); // => true

const properConfig = restrict(tooMuchConfig, config);
console.log(properConfig === config); // => false
console.log("vars" in properConfig); // => false
console.log("env" in properConfig); // => false

for(let [key, value] of Object.entries(config))
    {
        console.log(key + " -- " + value)

    }
console.log("***********************");
for(let [key, value] of Object.entries(tooMuchConfig))
    {
        console.log(key + " -- " + value)

    }
console.log("***********************");
for(let [key, value] of Object.entries(properConfig))
    {
        console.log(key + " -- " + value)

    }
