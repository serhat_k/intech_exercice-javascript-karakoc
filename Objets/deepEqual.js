function deepEqual(t1, t2)
{
    if([...t1] === [...t2])
    {
        return true
    }
    console.log(...t1);
    console.log(...t2);
    return false
}

const a1 = [1, 2, [3, 4], 5];
const a3 = [...a1]

console.log("a1 : " + a1);

const a2 = JSON.parse(JSON.stringify(a1)); // Deep copy
console.log("a2 : " + a2);
console.log("resultat comparaison a1 et a2 : " + (a1 === a2)); // => false
console.log("resultat de deepEqual : " + deepEqual(a1, a2)) // => true
console.log()

console.log("resultat comparaison a1 et a3 : " + (a1 === a3))
console.log("resultat de deepEqual : " + deepEqual(a1,a3))
console.log()

console.log("resultat comparaison a1 et a1 : " + (a1 === a1))
console.log("resultat de deepEqual : " + deepEqual(a1,a1))