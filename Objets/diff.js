
function diff(source, propertyNames) 
{
    console.log("####### Dans la fonction >>>>>>>>>>>>>>>>>")
    obj_return = {...source};
    console.log(propertyNames)
    console.log(obj_return)
    console.log(obj_return.hasOwnProperty(propertyNames));

    console.log("# Parcours de la source >")
    for (let [key, value] of Object.entries(obj_return)) {
        console.log("# Parcours de l'objet à retirer >")
        for (let [keyP, valueP] of Object.entries(propertyNames)) {
            console.log("clé de l'objet -> " + keyP + " = " + key + " <- clé de l'objet source ")
            if(keyP === key)
            {
                console.log("Résultat du delete : " + delete obj_return[key])
            }
        }
    }
    console.log(obj_return)
    // return obj_return;
    console.log("####### Fin de fonction RETURN >")
    return obj_return
}

let o1 = { r: 0, g: 0, b: 0, a: 0 };
console.log("contenu objet source o1 -> " + o1.r + o1.g + o1.b + o1.a)
let withoutOpacity = { a: null };

console.log("mise en fonction diff __");
let objectWithoutOpacity = diff(o1, withoutOpacity); // => { r: 0, g: 0, b: 0 }

console.log("Parcours de l'objet après diff >")
for (let [keyP, valueP] of Object.entries(objectWithoutOpacity)) {
    console.log(keyP)
}

console.log("résultat de l'égalité : " + o1 === objectWithoutOpacity); // => false