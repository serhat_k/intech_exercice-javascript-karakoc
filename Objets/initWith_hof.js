function initWith(size, f) {
    tabResult = []
    for(i = 0; i < size; i++)
    {
        tabResult.push(f(i))
    }
    return tabResult;
}

const initWithZeros = (size) => initWith(size,() => 0);
const initFrom = (size, start) => initWith(size,index => start + index);

console.log(initWithZeros(3)); // [0, 0, 0]
console.log(initFrom(3, 42)); // [42, 43, 44]