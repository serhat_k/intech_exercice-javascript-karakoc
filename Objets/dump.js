function dump(obj)
{
    for (let [key, value] of Object.entries(obj)) {
        console.log(key, value);
    }
}

let obj = {};
obj.firstname = "Alan";
obj.lastname = "Turing";
obj.birthday = [1921, 6, 23];
dump(obj); // => {firstname:"Alan",lastname:"Turing",birthday:[1921,6,23]}