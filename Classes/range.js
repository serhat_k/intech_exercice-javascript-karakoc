class Range {
    // Initialise l'objet
    constructor(from, to){
        this.from = from;
        this.to = to;
    }

    // Dit si `x` tombe dans l'interval [from; to]
    includes(x) {
        return (this.from <= x && this.to >= x)
    }

    // Produit une chaîne de caractère sous la forme "(from...to)", par exemple "(2...8)"
    toString(){
        return (this.from + "..." + this.to);
    }

    // Analyse une chaîne de caractère au format "(from...to)" et retourne une nouvelle instance
    static parse(s){        
        if (this.integerRangePattern.exec(s) !== null) 
            return new Range(this.takeLeft.exec(s)[0], this.takeRight.exec(s)[0])
        else 
            return false
    }

    // RegExp pour analyser la chaîne avec parse
    static integerRangePattern = RegExp(/^[0-9]+\.\.\.[0-9]+$/);
    static takeLeft = RegExp(/^[0-9]+/);
    static takeRight= RegExp(/[0-9]+$/);
    
}

class Span extends Range
{
    // Initialise l'objet
    constructor(from, to){
        super();
        if (from+to > from)
        {
            this.from = from;
            this.to = from + to;
        }
        else 
        {
            this.from = from + to;
            this.to = from;
        }
    }
}

testRange = new Range(1, 5);
console.log(testRange.includes(3));
console.log(testRange.includes(10));

console.log(testRange.toString());

test2 = Range.parse('40...50');
console.log(test2.includes(3));
console.log(test2.includes(45));
console.log(test2.includes(70));

console.log(test2.toString())

console.log("############################################")

console.log((new Span(2, 4)).toString()); // => "(2...6)"
console.log((new Span(12, -8)).toString()); // => "(4...12)"