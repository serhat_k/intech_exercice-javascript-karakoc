var start = document.getElementById("start-game");
start.addEventListener("click", () => {
    // chemin de l'image
    var img_path = 'image.jpg'; //'
    var img = new Image();
     
    // lorsque l'image est chargée
    img.onload = function ( )
    {
        // on prend le minimum largeur / hauteur au cas
        // où l'image ne serait pas carrée
        var min  = Math.min( img.width, img.height );
        //var min = 400
        // on divise cette taille par 3 (le nombre de
        // pièces sur 1 côté et on le prend l'entier
        // inférieur le plus proche car c'est un nombre
        // de pixel et qu'il ne faut pas déborder
        //
        // size = (le nombre de pixel d'un côté d'une pièce)
        var size = Math.floor( min / Number(document.getElementById("cells-quantity").value) );
         
        var i;
        var div;
        var row;
        var col;
         
        // le container est une div qui contiendra le puzzle
        var container = document.createElement('div');
         
        // 'on affiche les pièces une à une
        for ( i = 0; i < (Number(document.getElementById("cells-quantity").value) * Number(document.getElementById("cells-quantity").value)); i += 1 )
        {
            // Ici on va dire qu'une pièce de
            // puzzle c'est une <div>
            div = document.createElement('div');
             
            div.style.width  = size + 'px';
            div.style.height = size + 'px';
             
            //div.style.rotate = "90deg";
            div.style.backgroundImage = 'url(' + img_path + ')';
            div.style.rotate = Math.random()*360 + "deg"
            // L'astuce est ici
            // l'index de la pièce en x est : ( i % 3 )
            // l'index de la pièce en y est : Math.floor( i / 3 )
            // on les multiplis par size pour obtenir la position
            // du fond
            row = ( i % Number(document.getElementById("cells-quantity").value) );
            col = Math.floor( i / Number(document.getElementById("cells-quantity").value) );
             
            // A noter qu'il faut les opposer ( * -1 )pour obtenir
            // une translation correcte
            div.style.backgroundPosition =   - row * size + 'px ' + - col * size + 'px';
             
            // j'utilise les flotants pour positionner mon image
            div.style.float = 'left';
             
            // 'du coup si la ligne vaut 0, on revient à la ligne
            if ( !row )
            {
                div.style.clear = 'left';
            }
            
            div.addEventListener("click", () => {
                console.log(this)
                console.log("click div")
                console.log("before : " + this.style.rotate)
                if (Number(this.style.rotate.split("deg")[0]) !== 0)
                {
                    this.style.rotate = String(Number(this.style.rotate.split("deg")[0])+1) + "deg";
                    console.log("after : " + this.style.rotate)
                }
            });
            // on ajoute la pièce au puzzle
            container.appendChild( div );
        }
         
        // on ajoute le puzzle au document
        document.body.appendChild( container );
    };
     
    // chemin de l'image -> lance le téléchargement
    img.src = img_path;
});

document.getElementById("resolve").addEventListener("click", () => {

});

