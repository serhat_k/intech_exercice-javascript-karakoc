function titleUpperCase()
{
    for(t = 1; t <= 6; t++)
    {
        allTitle = document.getElementsByTagName("h" + t);
        
        for(i = 0;  i < allTitle.length; i++)
        {
            title = allTitle[i].textContent;
            allTitle[i].textContent = "";

            tabTitle = [... title];

            tabTitle.forEach(titleLetter => {
                allTitle[i].textContent += titleLetter.toUpperCase();
            });
        
    
        }
    }
}